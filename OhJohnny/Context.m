//
//  Context.m
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import "Context.h"

@implementation Context

-(id)init
{
    self = [super init];
    if(self)
    {
        self.nbrPts = 0;
        self.recordPts = 0;
        [self loadUserDefaults];
    }
    return self;
}

-(void)loadUserDefaults
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSInteger recPts = [defaults integerForKey:@"recordPts"];
    if(recPts != 0)
        self.recordPts = recPts;
}

@end
