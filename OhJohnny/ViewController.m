//
//  ViewController.m
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import "ViewController.h"
#import "GameViewController.h"

@interface ViewController ()

@end

@implementation ViewController

-(void)loadView
{
    [super loadView];
    
    [self.view setBackgroundColor:[UIColor grayColor]];
    
    //Label Titre
    //[self addTitle];
    
    [self addButton];
    [self recordPts];
    [self addSocialBtn];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    NSString *texteLabel = [NSString stringWithFormat:@"Record du nombre de points : %d", APPCONTEXT.recordPts];
    [self.labelRecord setText:texteLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View

-(void)addTitle
{
    CGFloat widthView = [Utils getScreenWidth];
    UIFont *font = [UIFont fontWithName:DEFAULT_FONT size:30];
    UILabel *labelTitle = [[UILabel alloc] init];
    [labelTitle setText:@"OhJohnny"];
    [labelTitle setFont:font];
    [labelTitle setTextColor:[UIColor blackColor]];
    [labelTitle setBackgroundColor:[UIColor clearColor]];
    [labelTitle setTextAlignment:NSTextAlignmentCenter];
    [labelTitle setNumberOfLines:1];
    [labelTitle setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizeLabel = CGSizeMake(widthView, 100);
    CGSize labelSize = [labelTitle.text sizeWithFont:font constrainedToSize:constrainedSizeLabel lineBreakMode:labelTitle.lineBreakMode];
    
    [labelTitle setFrame:CGRectMake(0, 30, widthView, labelSize.height)];
    [self.view addSubview:labelTitle];
    [labelTitle release];
}

-(void)addButton
{
    CGFloat widthView = [Utils getScreenWidth];
    //CGFloat heightView = [Utils getScreenHeight]-20;
    UIFont *font = [UIFont fontWithName:DEFAULT_FONT size:50];
    NSString *texteBtn = @"Jouer";
    UIButton *btnJouer = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnJouer setBackgroundColor:[UIColor clearColor]];
    [btnJouer setTitle:texteBtn forState:UIControlStateNormal];
    [btnJouer.titleLabel setFont:font];
    [btnJouer.titleLabel setTextColor:[UIColor blueColor]];
    [btnJouer addTarget:self action:@selector(showGameVC) forControlEvents:UIControlEventTouchUpInside];
    
    CGSize constrainedSizeLabel = CGSizeMake(widthView, 100);
    CGSize labelSize = [texteBtn sizeWithFont:font constrainedToSize:constrainedSizeLabel lineBreakMode:NSLineBreakByTruncatingTail];
    CGFloat widthBtn = labelSize.width+20;
    CGFloat heightBtn = labelSize.height+20;
    CGFloat xBtn = (widthView-widthBtn)/2;
    //CGFloat yBtn = (heightView-heightBtn)/2;
    CGFloat yBtn = 30;
    [btnJouer setFrame:CGRectMake(xBtn, yBtn, widthBtn, heightBtn)];
    [self.view addSubview:btnJouer];
    [btnJouer release];
}

-(void)recordPts
{
    CGFloat widthView = [Utils getScreenWidth];
    UIFont *font = [UIFont fontWithName:@"Verdana" size:15];
    self.labelRecord = [[UILabel alloc] init];
    NSString *texteLabel = [NSString stringWithFormat:@"Record du nombre de points : %d", APPCONTEXT.recordPts];
    [self.labelRecord setText:texteLabel];
    [self.labelRecord setFont:font];
    [self.labelRecord setTextColor:[UIColor whiteColor]];
    [self.labelRecord setBackgroundColor:[UIColor clearColor]];
    [self.labelRecord setTextAlignment:NSTextAlignmentCenter];
    [self.labelRecord setNumberOfLines:1];
    [self.labelRecord setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizeLabel = CGSizeMake(widthView, 100);
    CGSize labelSize = [self.labelRecord.text sizeWithFont:font constrainedToSize:constrainedSizeLabel lineBreakMode:self.labelRecord.lineBreakMode];
    
    [self.labelRecord setFrame:CGRectMake(0, 100, widthView, labelSize.height)];
    [self.view addSubview:self.labelRecord];
}

-(void)addSocialBtn
{
    //Label
    CGFloat widthView = [Utils getScreenWidth];
    UIFont *font = [UIFont fontWithName:DEFAULT_FONT size:20];
    UILabel *labelSocial = [[UILabel alloc] init];
    NSString *texteLabel = @"Contacter le grand Johnny :";
    [labelSocial setText:texteLabel];
    [labelSocial setFont:font];
    [labelSocial setTextColor:[UIColor whiteColor]];
    [labelSocial setBackgroundColor:[UIColor clearColor]];
    [labelSocial setTextAlignment:NSTextAlignmentCenter];
    [labelSocial setNumberOfLines:1];
    [labelSocial setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizelabel = CGSizeMake(widthView, 40);
    CGSize labelSize = [labelSocial.text sizeWithFont:font constrainedToSize:constrainedSizelabel lineBreakMode:labelSocial.lineBreakMode];
    CGFloat yLabel = self.labelRecord.frame.origin.y+self.labelRecord.frame.size.height+30;
    [labelSocial setFrame:CGRectMake(0, yLabel, widthView, labelSize.height)];
    
    [self.view addSubview:labelSocial];
    [labelSocial release];
    
    //Btn Fb & Twitter
    CGFloat yBtn = yLabel+labelSize.height+15;
    CGFloat widthBtn = 100;
    
    UIImage *imageFB = [UIImage imageNamed:@"fbLogo"];
    UIImage *imageTwitter = [UIImage imageNamed:@"twitterLogo"];
    UIButton *btnFB = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFB setBackgroundImage:imageFB forState:UIControlStateNormal];
    [btnFB addTarget:self action:@selector(btnSocialClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnFB setTag:10];
    [btnFB setFrame:CGRectMake(30, yBtn, widthBtn, widthBtn)];
    UIButton *btnTwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnTwitter setBackgroundImage:imageTwitter forState:UIControlStateNormal];
    [btnTwitter addTarget:self action:@selector(btnSocialClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnTwitter setTag:20];
    [btnTwitter setFrame:CGRectMake(widthView-widthBtn-30, yBtn, widthBtn, widthBtn)];
    
    [self.view addSubview:btnFB];
    [self.view addSubview:btnTwitter];
}

#pragma mark - Actions

-(void)showGameVC
{
    GameViewController *gvc = [[GameViewController alloc] init];
    [self.navigationController pushViewController:gvc animated:YES];
}

-(void)btnSocialClick:(id)sender
{
    UIButton *bt = (UIButton *) sender;
    switch (bt.tag) {
        case 10:
            //FB
            [self openFB];
            break;
        case 20:
            //Twitter
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/JohnnySjh"]];
            break;
        default:
            break;
    }
}

-(void)openFB
{
    NSURL *urlApp = [NSURL URLWithString:@"fb://"];
    BOOL canOpenFBApp = [[UIApplication sharedApplication] canOpenURL:urlApp];
    if(canOpenFBApp)
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://profile/366036613472952"]];
    else
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/jhofficiel"]];
}

@end
