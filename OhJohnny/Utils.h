//
//  Utils.h
//  Do The Harlem Shake
//
//  Created by Pierre Gilardin on 02/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+(BOOL)isIphone5;
+(int)getScreenWidth;
+(int)getScreenHeight;
+(void)showListFonts;
+(void)showError:(NSString *)errorString;

@end
