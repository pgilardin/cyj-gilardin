//
//  ViewController.h
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : OJViewController

@property (nonatomic, retain) UILabel *labelRecord;

@end
