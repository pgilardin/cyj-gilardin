//
//  AppDelegate.h
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Context.h"

#define APPCONTEXT [AppDelegate sharedAppDelegate].context
#define DEFAULT_FONT @"orangejuice"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;
@property (retain, nonatomic) Context *context;

+ (AppDelegate *)sharedAppDelegate;

@end
