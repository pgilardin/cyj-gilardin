//
//  GameViewController.h
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : OJViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *arrayTitres;
@property (nonatomic) NSInteger rowMauvaisTitre;
@property (nonatomic) NSInteger nbrReponses;
@property (nonatomic) NSInteger nbrPts;
@property (nonatomic, retain) UIView *resultView;
@property (nonatomic, retain) UIButton *btn50;
@property (nonatomic, retain) UIButton *btnJo;
@property (nonatomic, retain) UIButton *btnPublic;

@end
