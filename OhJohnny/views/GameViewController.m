//
//  GameViewController.m
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import "GameViewController.h"

#define CELL_HEIGHT 40
#define NUMBER_CELL 4

@interface GameViewController ()

@end

@implementation GameViewController

-(id)init
{
    self = [super init];
    if(self)
    {
        self.nbrReponses = NUMBER_CELL;
        self.nbrPts = 0;
        [self loadConfig];
    }
    return self;
}

-(void)dealloc
{
    [self.tableView release];
    [self.arrayTitres release];
    [self.resultView release];
    [self.btn50 release];
    [self.btnJo release];
    [self.btnPublic release];
    [super dealloc];
}

-(void)loadView
{
    [super loadView];
    [self.view setBackgroundColor:[UIColor blackColor]];
    [self addTableView];
    [self addLabel];
    [self addJokers];
    
}

#pragma mark - Configuration

-(void)loadConfig
{
    self.arrayTitres = [[NSMutableArray alloc] init];
    //Déterminer le rang du mauvais titre
    self.rowMauvaisTitre = arc4random() % self.nbrReponses;
    NSLog(@"rang mauvais titre : %d", self.rowMauvaisTitre);
    
    //Charger les titres
    NSString *path = [[NSBundle mainBundle] pathForResource:@"listeTitres" ofType:@"plist"];
	NSData *plistData = [NSData dataWithContentsOfFile:path];
	NSString *error; NSPropertyListFormat format;
	NSDictionary *dict = [NSPropertyListSerialization propertyListFromData:plistData
														  mutabilityOption:NSPropertyListImmutable
																	format:&format
														  errorDescription:&error];
	
	if (dict) {
        //Choix du mauvais titre
        NSString *titreMauvais = nil;
        NSInteger rowToPickMauvais = arc4random() % [[dict objectForKey:@"bad"] count];
        titreMauvais = [[dict objectForKey:@"bad"] objectAtIndex:rowToPickMauvais];
        
        //Choix des bons titres
        NSMutableArray *arrayBonTitres = [[NSMutableArray alloc] initWithArray:[dict objectForKey:@"good"]];
        for(int i=0; i<self.nbrReponses-1;i++)
        {
            NSInteger rowToPickGood = arc4random() % [arrayBonTitres count];
            [self.arrayTitres addObject:[arrayBonTitres objectAtIndex:rowToPickGood]];
            [arrayBonTitres removeObjectAtIndex:rowToPickGood];
        }
        
        [self.arrayTitres insertObject:titreMauvais atIndex:self.rowMauvaisTitre];
	}
}

#pragma mark - View

-(void)addTableView
{
    //Ajout du tableview
    CGFloat widthTV = [Utils getScreenWidth];
    CGFloat heightTV = self.nbrReponses * CELL_HEIGHT;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, widthTV, heightTV)];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView setScrollEnabled:NO];
    //[self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableView];
    //[self.view setBackgroundColor:[UIColor blackColor]];
}

-(void)addLabel
{
    CGFloat widthView = [Utils getScreenWidth]-10;
    UIFont *font = [UIFont fontWithName:@"Optima-Bold" size:20];
    UILabel *labelDescription = [[UILabel alloc] init];
    NSString *texteDescription = @"Parmis ces différentes chansons, laquelle n'est pas de Johnny ?";
    [labelDescription setText:texteDescription];
    //[labelDescription setBackgroundColor:[UIColor whiteColor]];
    [labelDescription setFont:font];
    [labelDescription setTextColor:[UIColor whiteColor]];
    [labelDescription setBackgroundColor:[UIColor clearColor]];
    [labelDescription setTextAlignment:NSTextAlignmentLeft];
    [labelDescription setNumberOfLines:0];
    [labelDescription setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizeLabel = CGSizeMake(widthView, 100);
    CGSize labelSize = [labelDescription.text sizeWithFont:font constrainedToSize:constrainedSizeLabel lineBreakMode:labelDescription.lineBreakMode];
    
    [labelDescription setFrame:CGRectMake(5, 0, widthView, labelSize.height)];
    [self.view addSubview:labelDescription];
    [labelDescription release];
}

-(void)addJokers
{
    CGFloat xView = 0;
    CGFloat yView = self.tableView.frame.size.height+self.tableView.frame.origin.y+20;
    CGFloat widthView = [Utils getScreenWidth];
    CGFloat heightVisibleView = [Utils getScreenHeight]-20;
    CGFloat heightView = heightVisibleView-yView;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xView, yView, widthView, heightView)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    //Label
    CGFloat xLabel = 0;
    CGFloat yLabel = 0;
    UIFont *font = [UIFont fontWithName:@"Optima-Bold" size:20];
    UILabel *label = [[UILabel alloc] init];
    NSString *texteLabel = @"Jokers :";
    [label setText:texteLabel];
    [label setFont:font];
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setNumberOfLines:1];
    [label setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizeLabel = CGSizeMake(widthView, 100);
    CGSize labelSize = [label.text sizeWithFont:font constrainedToSize:constrainedSizeLabel lineBreakMode:label.lineBreakMode];
    [label setFrame:CGRectMake(xLabel, yLabel, widthView, labelSize.height)];
    [view addSubview:label];
    [label release];
    
    UIImage *imageBtn = [[UIImage imageNamed:@"blueButton.png"]
                         resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *imageBtnDisabled = [[UIImage imageNamed:@"greyButton.png"]
                                 resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    CGFloat heightBtn = 40;
    //Btn 50/50
    CGFloat xBtn50 = 30;
    CGFloat yBtn50 = yLabel+labelSize.height+10;
    CGFloat widthBtn50 = 80;
    
    self.btn50 = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btn50 setBackgroundImage:imageBtn forState:UIControlStateNormal];
    [self.btn50 setBackgroundImage:imageBtnDisabled forState:UIControlStateDisabled];
    [self.btn50 setTitle:@"50/50" forState:UIControlStateNormal];
    [self.btn50 setTag:10];
    [self.btn50 addTarget:self action:@selector(clicJoker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btn50 setFrame:CGRectMake(xBtn50, yBtn50, widthBtn50, heightBtn)];
    [view addSubview:self.btn50];
    
    //Btn Appel à Johnny
    CGFloat widthBtnJo = 150;
    CGFloat xBtnJo = widthView-widthBtnJo-30;
    
    self.btnJo = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnJo setBackgroundImage:imageBtn forState:UIControlStateNormal];
    [self.btnJo setBackgroundImage:imageBtnDisabled forState:UIControlStateDisabled];
    [self.btnJo setTitle:@"Appel à Johnny" forState:UIControlStateNormal];
    [self.btnJo setTag:20];
    [self.btnJo addTarget:self action:@selector(clicJoker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnJo setFrame:CGRectMake(xBtnJo, yBtn50, widthBtnJo, heightBtn)];
    [view addSubview:self.btnJo];
    
    //Btn Avis du Public
    CGFloat widthBtnPublic = 150;
    CGFloat yBtnPublic = yBtn50+heightBtn+20;
    CGFloat xBtnPublic = (widthView-widthBtnPublic)/2;
    
    self.btnPublic = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnPublic setBackgroundImage:imageBtn forState:UIControlStateNormal];
    [self.btnPublic setBackgroundImage:imageBtnDisabled forState:UIControlStateDisabled];
    [self.btnPublic setTitle:@"Avis du public" forState:UIControlStateNormal];
    [self.btnPublic setTag:30];
    [self.btnPublic addTarget:self action:@selector(clicJoker:) forControlEvents:UIControlEventTouchUpInside];
    [self.btnPublic setFrame:CGRectMake(xBtnPublic, yBtnPublic, widthBtnPublic, heightBtn)];
    [view addSubview:self.btnPublic];
    
    [self.view addSubview:view];
    [view release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.nbrReponses;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell.textLabel setText:[self.arrayTitres objectAtIndex:indexPath.row]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

#pragma mark - Tableview Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(![cell.textLabel.text isEqualToString:@""])
    {
        if(indexPath.row == self.rowMauvaisTitre)
        {
            [cell setBackgroundColor:[UIColor greenColor]];
            [self showSuccess];
        }
        else
        {
            [cell setBackgroundColor:[UIColor redColor]];
            [self showError];
        }
    }
    NSLog(@"row : %d", indexPath.row);
}

#pragma mark - Gestion Résultat

-(void)showError
{
    [self showResultView:NO];
}

-(void)showSuccess
{
    self.nbrPts += 100;
    
    if(![self.btn50 isEnabled])
        self.nbrPts -= 25;
    if(![self.btnJo isEnabled])
        self.nbrPts -= 25;
    if(![self.btnPublic isEnabled])
        self.nbrPts -=25;
    
    if(self.nbrPts > APPCONTEXT.recordPts)
    {
        //Sauvegarde du record
        APPCONTEXT.recordPts = self.nbrPts;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:APPCONTEXT.recordPts forKey:@"recordPts"];
        [defaults synchronize];
    }
    [self showResultView:YES];
}

-(void)showResultView:(BOOL)isSuccess
{
    CGFloat widthView = [Utils getScreenWidth];
    CGFloat heightView = [Utils getScreenHeight]-20;
    CGFloat marge = 20;
    CGFloat widthResultView = widthView-2*marge;
    CGFloat heightResultView = heightView-2*marge;
    
    UIColor *colorView = nil;
    if(isSuccess)
        colorView = [UIColor colorWithRed:0 green:0.255 blue:0 alpha:0.9];
    else
        colorView = [UIColor colorWithRed:0.255 green:0 blue:0 alpha:0.9];
    
    self.resultView = [[UIView alloc] initWithFrame:CGRectMake(marge, marge, widthResultView, heightResultView)];
    [self.resultView setBackgroundColor:colorView];
    
    //Label Resultat
    CGFloat xLabelResultat = 0;
    CGFloat yLabelResultat = 10;
    UIFont *fontResultat = [UIFont fontWithName:DEFAULT_FONT size:25];
    UILabel *labelResultat = [[UILabel alloc] init];
    NSString *texteResultat = nil;
    if(isSuccess)
        texteResultat = @"GAGNE";
    else
        texteResultat = @"PERDU";
    [labelResultat setText:texteResultat];
    [labelResultat setFont:fontResultat];
    [labelResultat setTextColor:[UIColor whiteColor]];
    [labelResultat setBackgroundColor:[UIColor clearColor]];
    [labelResultat setTextAlignment:NSTextAlignmentCenter];
    [labelResultat setNumberOfLines:1];
    [labelResultat setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizeLabelResultat = CGSizeMake(widthView, 100);
    CGSize labelSizeResultat = [labelResultat.text sizeWithFont:fontResultat constrainedToSize:constrainedSizeLabelResultat lineBreakMode:labelResultat.lineBreakMode];
    
    [labelResultat setFrame:CGRectMake(xLabelResultat, yLabelResultat, widthResultView, labelSizeResultat.height)];
    NSLog(@"labelsize height : %f", labelSizeResultat.height);
    NSLog(@"labelsize width : %f", labelSizeResultat.width);
    [self.resultView addSubview:labelResultat];
    [labelResultat release];
    
    //Tête de johnny
    UIImage *teteDeJo = nil;
    if(isSuccess)
        teteDeJo = [UIImage imageNamed:@"goodJo"];
    else
        teteDeJo = [UIImage imageNamed:@"badJo"];
    
    UIImageView *teteDeJoView = [[UIImageView alloc] initWithImage:teteDeJo];
    CGFloat widthTDJView = teteDeJoView.frame.size.width;
    CGFloat heightTDJView = teteDeJoView.frame.size.height;
    CGFloat xTDJView = (widthResultView-widthTDJView)/2;
    CGFloat yTDJView = yLabelResultat+30;
    [teteDeJoView setFrame:CGRectMake(xTDJView, yTDJView, widthTDJView, heightTDJView)];
    [self.resultView addSubview:teteDeJoView];
    [teteDeJoView release];
    
    //Btn Rejouer
    CGFloat heightBtnReplay = 0;
    CGFloat yBtnReplay = yTDJView+heightTDJView;
    CGFloat widthBtn = 150;
    CGFloat xBtn = (widthResultView-widthBtn)/2;
    if(isSuccess)
    {
        yBtnReplay += 20;
        
        
        heightBtnReplay = 40;
        UIImage *imageBtnReplay = [[UIImage imageNamed:@"blueButton.png"]
                                   resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
        UIButton *btnReplay = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnReplay setBackgroundImage:imageBtnReplay forState:UIControlStateNormal];
        [btnReplay setTitle:@"Continuer à jouer" forState:UIControlStateNormal];
        [btnReplay addTarget:self action:@selector(continueToPlay) forControlEvents:UIControlEventTouchUpInside];
        [btnReplay setFrame:CGRectMake(xBtn, yBtnReplay, widthBtn, heightBtnReplay)];
        [self.resultView addSubview:btnReplay];
    }
    
    //Btn Quit
    CGFloat heightBtnQuit = 40;
    CGFloat yBtnQuit = yBtnReplay+heightBtnReplay+20;
    UIImage *imageBtnQuit = [[UIImage imageNamed:@"orangeButton.png"]
                             resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIButton *btnQuit = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnQuit setBackgroundImage:imageBtnQuit forState:UIControlStateNormal];
    [btnQuit setTitle:@"Quitter" forState:UIControlStateNormal];
    [btnQuit addTarget:self action:@selector(quitGame) forControlEvents:UIControlEventTouchUpInside];
    [btnQuit setFrame:CGRectMake(xBtn, yBtnQuit, widthBtn, heightBtnQuit)];
    [self.resultView addSubview:btnQuit];
    
    //Nbr de points total
    CGFloat xLabelNbrPts = 0;
    CGFloat yLabelNbrPts = yBtnQuit+heightBtnQuit+30;
    UIFont *font = [UIFont fontWithName:@"Verdana" size:15];
    UILabel *labelNbrPts = [[UILabel alloc] init];
    NSString *texteNbrPts = [NSString stringWithFormat:@"Nombre total de points : %d", self.nbrPts];
    [labelNbrPts setText:texteNbrPts];
    [labelNbrPts setFont:font];
    [labelNbrPts setTextColor:[UIColor whiteColor]];
    [labelNbrPts setBackgroundColor:[UIColor clearColor]];
    [labelNbrPts setTextAlignment:NSTextAlignmentCenter];
    [labelNbrPts setNumberOfLines:1];
    [labelNbrPts setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizeLabel = CGSizeMake(widthView, 100);
    CGSize labelSize = [labelNbrPts.text sizeWithFont:font constrainedToSize:constrainedSizeLabel lineBreakMode:labelNbrPts.lineBreakMode];
    
    [labelNbrPts setFrame:CGRectMake(xLabelNbrPts, yLabelNbrPts, widthResultView, labelSize.height)];
    [self.resultView addSubview:labelNbrPts];
    [labelNbrPts release];
    
    [self.view addSubview:self.resultView];
}

#pragma mark - Actions des Btns

-(void)clicJoker:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 10:
            [self do5050];
            break;
        case 20:
            [self appelJohnny];
            break;
        case 30:
            [self avisPublic];
            break;
            
        default:
            break;
    }
    [btn setEnabled:NO];
}

-(void)continueToPlay
{
    [self.resultView removeFromSuperview];
    [self loadConfig];
    [self.tableView reloadData];
    [self.btnPublic setEnabled:YES];
    [self.btnJo setEnabled:YES];
    [self.btn50 setEnabled:YES];
}

-(void)quitGame
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Fcts Joker

-(void)avisPublic
{
    BOOL success = FALSE;
    do
    {
        NSInteger randomRow = arc4random() % self.nbrReponses;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:randomRow inSection:0];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if(![cell.textLabel.text isEqualToString:@""])
        {
            success = TRUE;
            [cell setBackgroundColor:[UIColor yellowColor]];
        }
    }while(!success);
}

-(void)appelJohnny
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message de Johnny"
                                                    message:@"J'en sais rien, j'les connais pas toutes !"
                                                   delegate:nil
                                          cancelButtonTitle:@"Merci"
                                          otherButtonTitles: nil];
    [alert show];
    [alert release];
}

-(void)do5050
{
    NSInteger nbrSuppr = 0;
    do {
        NSInteger randomRow = arc4random() % self.nbrReponses;
        if(randomRow != self.rowMauvaisTitre)
        {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:randomRow inSection:0]];
            if(![cell.textLabel.text isEqualToString:@""])
            {
                nbrSuppr++;
                [cell.textLabel setText:@""];
            }
        }
    } while (nbrSuppr != 2);
}

@end
