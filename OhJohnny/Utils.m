//
//  Utils.m
//  Do The Harlem Shake
//
//  Created by Pierre Gilardin on 02/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(BOOL)isIphone5
{
    if ([self getScreenHeight] > 500)
        return YES;
    else
        return NO;
}

#pragma mark - Screen Size

+(int)getScreenWidth
{
    int w = [UIScreen mainScreen].bounds.size.width;
    return w;
}

+(int)getScreenHeight
{
    int h = [UIScreen mainScreen].bounds.size.height;
    return h;
}

#pragma mark - Font

+(void)showListFonts
{
    //Font
    for (NSString *familyName in [UIFont familyNames])
    {
        NSLog(@"----------------");
        NSLog(@"FAMILY: %@", familyName);
        for(NSString *fontName in [UIFont fontNamesForFamilyName:familyName])
        {
            NSLog(@"  %@", fontName);
        }
    }
}

#pragma mark - AlertView Error

+(void)showError:(NSString *)errorString
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur"
                                                    message:errorString
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
    [alert release];
}

@end
