//
//  OJViewController.m
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import "OJViewController.h"

@interface OJViewController ()

@end

@implementation OJViewController

-(id)init{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}

-(void)loadView
{
    [super loadView];
    [self setNavBar];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Set Navbar

-(void)setNavBar
{
    UIView *view = [[UIView alloc] init];
    
    //Image Johnny
    UIImage *titleImage = [UIImage imageNamed:@"jHead"];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:titleImage];
    CGFloat yImage = (44-titleImageView.frame.size.height)/2;
    CGRect titleImageViewFrame = CGRectMake(0, yImage, titleImageView.frame.size.width, titleImageView.frame.size.height);
    [titleImageView setFrame:titleImageViewFrame];
    
    
    
    //Label Titre
    CGFloat widthView = [Utils getScreenWidth];
    UIFont *font = [UIFont fontWithName:DEFAULT_FONT size:25];
    UILabel *labelTitle = [[UILabel alloc] init];
    [labelTitle setText:@"OhJohnny"];
    [labelTitle setFont:font];
    [labelTitle setTextColor:[UIColor whiteColor]];
    [labelTitle setBackgroundColor:[UIColor clearColor]];
    [labelTitle setTextAlignment:NSTextAlignmentCenter];
    [labelTitle setNumberOfLines:1];
    [labelTitle setLineBreakMode:NSLineBreakByTruncatingTail];
    
    CGSize constrainedSizeLabel = CGSizeMake(widthView, 100);
    CGSize labelSize = [labelTitle.text sizeWithFont:font constrainedToSize:constrainedSizeLabel lineBreakMode:labelTitle.lineBreakMode];
    
    CGFloat xLabel = titleImageViewFrame.size.width+5;
    
    [labelTitle setFrame:CGRectMake(xLabel, yImage, labelSize.width, titleImageViewFrame.size.height)];
    
    
    [view setFrame:CGRectMake(0, 0, xLabel+labelSize.width, 44)];
    NSLog(@"view width %f", xLabel+labelSize.width);
    [view addSubview:titleImageView];
    [titleImageView release];
    [view addSubview:labelTitle];
    [labelTitle release];
    
    [self.navigationItem setTitleView:view];
    
}

@end
