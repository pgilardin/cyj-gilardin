//
//  Context.h
//  OhJohnny
//
//  Created by Pierre Gilardin on 23/03/13.
//  Copyright (c) 2013 Pierre Gilardin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Context : NSObject

@property (nonatomic) NSInteger nbrPts;
@property (nonatomic) NSInteger recordPts;

@end
